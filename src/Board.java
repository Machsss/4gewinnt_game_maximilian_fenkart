public class Board {

    String[] board = new String[30];

    public Board(){

    }

    public Board(String[] board){this.board = board;}

    public String[] initializeBoard(String[] board){
        for(int i = 0; i < 30; i++){
            this.board[i] = board[i];
        }

        return this.board;
    }

    public boolean checkMovesLeft(){
        for(int i = 0 ; i < this.board.length; i++){
            if(this.board[i] != "X" && this.board[i] != "O")
                return true;
        }

        return false;
    }

    public String[] getBoard() {
        return board;
    }

    public void setBoard(String[] board) {
        this.board = board;
    }

    public void setBoardPosition(int position,String input){
        this.board[position] = input;
    }
}
